const Education = require('../models/Education')
const educationService = {
  async getEducations (req, res, next) {
    try {
      const allEducation = await Education.find({})
      console.log(allEducation)
      res.json(allEducation)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getEducationId (req, res, next) {
    try {
      const { id } = req.params
      console.log(id)
      const educaiton = await Education.findById({ _id: id })
      console.log(educaiton)
      res.json(educaiton)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addEducation (req, res, next) {
    const payload = req.body
    console.log(payload)
    const education = new Education(payload)
    console.log(education)
    try {
      const newEducation = await education.save()
      res.json(newEducation)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateEducation (req, res, next) {
    const payload = req.body
    console.log('payloadddd')
    console.log(payload)
    try {
      const education = await Education.updateOne(
        { _id: payload._id },
        payload
      )
      console.log(education)
      res.json(education)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = educationService

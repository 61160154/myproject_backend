const Applicant = require('../models/Applicant')
const applicantService = {
  async getApplicants (req, res, next) {
    try {
      const allApplicant = await Applicant.find({})
      console.log(allApplicant)
      res.json(allApplicant)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getApplicantId (req, res, next) {
    try {
      const { id } = req.params
      console.log(id)
      const applicant = await Applicant.findById({ _id: id })
      console.log(applicant)
      res.json(applicant)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addApplicant (req, res, next) {
    const payload = req.body
    console.log(payload)
    const applicant = new Applicant(payload)
    console.log(applicant)
    try {
      const newApplicant = await applicant.save()
      res.json(newApplicant)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateApplicant (req, res, next) {
    const payload = req.body
    console.log('payloadddd')
    console.log(payload)
    try {
      const applicant = await Applicant.updateOne({ _id: payload._id }, payload)
      console.log(applicant)
      res.json(applicant)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = applicantService

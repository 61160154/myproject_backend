const Work = require('../models/Work')
const workService = {
  async getWorks (req, res, next) {
    try {
      const allWork = await Work.find({})
      console.log(allWork)
      res.json(allWork)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getWorkId (req, res, next) {
    try {
      const { id } = req.params
      console.log(id)
      const work = await Work.findById({ _id: id })
      console.log(work)
      res.json(work)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addWork (req, res, next) {
    const payload = req.body
    console.log(payload)
    const work = new Work(payload)
    console.log(work)
    try {
      const newWork = await work.save()
      res.json(newWork)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateWork (req, res, next) {
    const payload = req.body
    console.log('payloadddd')
    console.log(payload)
    try {
      const work = await Work.updateOne({ _id: payload._id }, payload)
      console.log(work)
      res.json(work)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = workService

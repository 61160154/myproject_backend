const Application = require('../models/Application')
const applicationService = {
  async getApplications (req, res, next) {
    try {
      const allApplication = await Application.find({})
      console.log(allApplication)
      res.json(allApplication)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getApplicationId (req, res, next) {
    try {
      const { id } = req.params
      console.log(id)
      const application = await Application.findById({ _id: id })
      console.log(application)
      res.json(application)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addApplication (req, res, next) {
    const payload = req.body
    console.log(payload)
    const application = new Application(payload)
    console.log(application)
    try {
      const newApplication = await application.save()
      res.json(newApplication)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateApplication (req, res, next) {
    const payload = req.body
    console.log('payloadddd')
    console.log(payload)
    try {
      const application = await Application.updateOne(
        { _id: payload._id },
        payload
      )
      console.log(application)
      res.json(application)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = applicationService

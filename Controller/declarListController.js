const DeclareS = require('../models/Declare')
const Company = require('../models/Company')
// const companyService = require('../Controller/companyController')

const Declare = {
  async addDeclare (req, res, next) {
    console.log('adddddddddddddddd')
    const payload = req.body
    console.log(payload)
    const declare = new DeclareS(payload)
    console.log(declare)
    try {
      const declareNew = await declare.save()
      res.json(declareNew)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateDeclare (req, res, next) {
    const payload = req.body
    try {
      const declare = await DeclareS.updateOne({ _id: payload._id }, payload)
      console.log(declare)
      res.json(declare)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteDeclare (req, res, next) {
    const { id } = req.params
    try {
      const declare = await DeclareS.deleteOne({ _id: id })
      res.json(declare)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getDeclareList (req, res, next) {
    try {
      const declares = await DeclareS.find({})
      res.json(declares)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getDeclareByCompanyId (req, res, next) {
    try {
      const { id } = req.params
      const declare = await DeclareS.find({ companyId: parseInt(id) })
      res.json(declare)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getLastDeclares (req, res, next) {
    try {
      const { id } = req.params
      const declareList = []
      const declareRecent = await DeclareS.find({ companyId: parseInt(id) })
      for (
        let index = declareRecent.length - 1;
        index >= declareRecent.length - 4;
        index--
      ) {
        declareList.push(declareRecent[index])
      }
      // console.log(declareList)
      res.json(declareList)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getDetailDeclareById (req, res, next) {
    try {
      const { id } = req.params
      // console.log(id)
      const detailDeclare = await DeclareS.findById({ _id: id })
      res.json(detailDeclare)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getDeclaresbangkok (req, res, next) {
    try {
      const { id } = req.params
      // console.log(id)
      const province = await Company.find({
        'addressCompany.province': 'กรุงเทพ'
      })
      const declare = await DeclareS.findById({ id: id })
      console.log(declare)
      res.json(province)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = Declare

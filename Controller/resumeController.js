const Resume = require('../models/Resume')
const resumeService = {
  async getResumes (req, res, next) {
    try {
      const allResume = await Resume.find({})
      console.log(allResume)
      res.json(allResume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getResumeId (req, res, next) {
    try {
      const { id } = req.params
      console.log(id)
      const resume = await Resume.findById({ _id: id })
      console.log(resume)
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addResume (req, res, next) {
    const payload = req.body
    console.log(payload)
    const resume = new Resume(payload)
    console.log(resume)
    try {
      const newResume = await resume.save()
      res.json(newResume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateResume (req, res, next) {
    const payload = req.body
    console.log('payloadddd')
    console.log(payload)
    try {
      const resume = await Resume.updateOne({ _id: payload._id }, payload)
      console.log(resume)
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = resumeService

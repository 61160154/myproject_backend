const Job = require('../models/JobRequirement')
const jobService = {
  async getJobs (req, res, next) {
    try {
      const allJob = await Job.find({})
      console.log(allJob)
      res.json(allJob)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobId (req, res, next) {
    try {
      const { id } = req.params
      console.log(id)
      const job = await Job.findById({ _id: id })
      console.log(job)
      res.json(job)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addJob (req, res, next) {
    const payload = req.body
    console.log(payload)
    const job = new Job(payload)
    console.log(job)
    try {
      const newJob = await job.save()
      res.json(newJob)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateJob (req, res, next) {
    const payload = req.body
    console.log('payloadddd')
    console.log(payload)
    try {
      const job = await Job.updateOne({ _id: payload._id }, payload)
      console.log(job)
      res.json(job)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = jobService

const Portfolio = require('../models/Application')
const portfolioService = {
  async getportfolios (req, res, next) {
    try {
      const allPortfolio = await Portfolio.find({})
      console.log(allPortfolio)
      res.json(allPortfolio)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getPortfolioId (req, res, next) {
    try {
      const { id } = req.params
      console.log(id)
      const portfolio = await Portfolio.findById({ _id: id })
      console.log(portfolio)
      res.json(portfolio)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async addPortfolio (req, res, next) {
    const payload = req.body
    console.log(payload)
    const portfolio = new Portfolio(payload)
    console.log(portfolio)
    try {
      const newPortfolio = await portfolio.save()
      res.json(newPortfolio)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updatePortfolio (req, res, next) {
    const payload = req.body
    console.log('payloadddd')
    console.log(payload)
    try {
      const portfolio = await Portfolio.updateOne({ _id: payload._id }, payload)
      console.log(portfolio)
      res.json(portfolio)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = portfolioService

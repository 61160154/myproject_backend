const mongoose = require('mongoose')
const declareSchema = new mongoose.Schema({
  id: Number,
  dateStart: String,
  dateClose: String,
  statusDeclare: String,
  position: {
    positionName: String,
    workType: String,
    gender: String,
    age: String,
    rate: Number,
    salary: Number,
    detail: String,
    property: [Array],
    note: ''
  },
  companyId: Number,
  company: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Company'
  }
})

module.exports = mongoose.model('Declare', declareSchema)

const mongoose = require('mongoose')
const applicantSchema = new mongoose.Schema({
  id: Number,
  m_User: String,
  m_Email: String,
  m_Password: String,
  m_Firstname: String,
  m_Lastname: String,
  m_Birthdate: String,
  m_Nationality: String,
  m_Religion: String,
  m_Gender: String,
  m_Weight: Number,
  m_Height: Number,
  m_Address: {
    m_Number: String,
    m_Lane: String,
    m_VillageNo: String,
    m_District: String,
    m_SubDistrict: String,
    m_Province: String,
    m_PostalCode: String
  },
  m_Phonenumber: String
})

module.exports = mongoose.model('Applicant', applicantSchema)

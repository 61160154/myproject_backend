const mongoose = require('mongoose')
const applicationSchema = new mongoose.Schema({
  id: Number,
  resumeId: Number,
  declareId: Number,
  statusApplication: String,
  statusInterview: {
    status: String,
    date: String,
    address: String,
    prepare: String
  }
})

module.exports = mongoose.model('Application', applicationSchema)

const mongoose = require('mongoose')
const companySchema = new mongoose.Schema({
  id: Number,
  companyName: String,
  styleBusiness: String,
  security: String,
  companyCode: String,
  contactName: String,
  detailCompany: String,
  telCompany: String,
  addressCompany: {
    number: String,
    lane: String,
    villageNo: String,
    district: String,
    subDistrict: String,
    province: String,
    postalCode: String
  },
  website: String,
  facebook: String,
  statusCheckCompany: String,
  username: String,
  password: String,
  email: String,
  status: String
})

module.exports = mongoose.model('Company', companySchema)

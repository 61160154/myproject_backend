const mongoose = require('mongoose')
const jobSchema = new mongoose.Schema({
  educationId: Number,
  resumeId: Number,
  currentState: String,
  leveofEducation: String,
  academy: String,
  educatonal: String,
  disciplines: String,
  grade: Number
})

module.exports = mongoose.model('Jobrequirement', jobSchema)

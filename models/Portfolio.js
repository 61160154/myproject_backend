const mongoose = require('mongoose')
const portfoliopersonScheme = new mongoose.Schema({
  portfolioId: 1,
  resumeId: 1,
  timeProfolio: 'ตั้งแต่ มกราคม 2564 จนถึง มกราคม 2564',
  institution: 'มหาวิทยาลัยบูรพา',
  course: 'บริหารธุรกิจบัณฑิต',
  certificate: '',
  vehicle: 'รถจักรยานยนต์',
  drivingAbility: 'รถจักรยานยนต์, รถยนต์'
})
module.exports = mongoose.model('Application', portfoliopersonScheme)

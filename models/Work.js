const mongoose = require('mongoose')
const workSchema = new mongoose.Schema({
  workhistoryId: Number,
  resumeId: Number,
  workExerience: Number,
  nameCompany: String,
  addressCompany: String,
  salary: Number,
  position: String,
  responsibiliti: String,
  time: String
})

module.exports = mongoose.model('workhistory', workSchema)

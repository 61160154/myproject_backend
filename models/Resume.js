const mongoose = require('mongoose')
const resumeSchema = new mongoose.Schema({
  id: Number,
  applicantId: Number,
  name: String
})

module.exports = mongoose.model('Resume', resumeSchema)

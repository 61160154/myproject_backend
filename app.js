const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')
const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const declareListRouter = require('./routes/declares')
const companyRouter = require('./routes/company')
const applicantRouter = require('./routes/applicant')
const applicationRouter = require('./routes/application')
const educationRouter = require('./routes/education')
const jobRouter = require('./routes/job')
const resumeRouter = require('./routes/resume')
const portfolioRouter = require('./routes/portfolio')
const workRouter = require('./routes/work')
const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/declare', declareListRouter)
app.use('/company', companyRouter)
app.use('/applicant', applicantRouter)
app.use('/application', applicationRouter)
app.use('/education', educationRouter)
app.use('/job', jobRouter)
app.use('/resume', resumeRouter)
app.use('/folio', portfolioRouter)
app.use('/work', workRouter)

module.exports = app

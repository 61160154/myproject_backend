const express = require('express')
const router = express.Router()
const applicantsController = require('../Controller/applicantsController')

router.get('/', applicantsController.getApplicants)

router.get('/:id', applicantsController.getApplicantId)

router.post('/', applicantsController.addApplicant)

router.put('/', applicantsController.updateApplicant)

// router.get('/count/:id', companyController.getCountId)

module.exports = router

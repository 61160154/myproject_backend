const express = require('express')
const router = express.Router()
const resumeController = require('../Controller/resumeController')

router.get('/', resumeController.getResumes)

router.get('/:id', resumeController.getResumeId)

router.post('/', resumeController.addResume)

router.put('/', resumeController.updateResume)

// router.get('/count/:id', companyController.getCountId)

module.exports = router

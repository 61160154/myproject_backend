const express = require('express')
const router = express.Router()
const applicationController = require('../Controller/applicationController')

router.get('/', applicationController.getApplications)

router.get('/:id', applicationController.getApplicationId)

router.post('/', applicationController.addApplication)

router.put('/', applicationController.updateApplication)

// router.get('/count/:id', companyController.getCountId)

module.exports = router

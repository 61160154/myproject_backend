const express = require('express')
const router = express.Router()
const WorkController = require('../Controller/workController')

router.get('/', WorkController.getWorks)

router.get('/:id', WorkController.getWorkId)

router.post('/', WorkController.addWork)

router.put('/', WorkController.updateWork)

// router.get('/count/:id', companyController.getCountId)

module.exports = router

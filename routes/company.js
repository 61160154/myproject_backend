const express = require('express')
const router = express.Router()
const companyController = require('../Controller/companyController')

router.get('/', companyController.getAllCompany)

router.get('/:id', companyController.getCompanyId)

router.post('/', companyController.addCompany)

router.put('/', companyController.updateCompany)

// router.get('/count/:id', companyController.getCountId)

module.exports = router

const express = require('express')
const router = express.Router()
const PortfolioController = require('../Controller/portfolioController')

router.get('/', PortfolioController.getportfolios)

router.get('/:id', PortfolioController.getPortfolioId)

router.post('/', PortfolioController.addPortfolio)

router.put('/', PortfolioController.updatePortfolio)

// router.get('/count/:id', companyController.getCountId)

module.exports = router

const express = require('express')
const router = express.Router()
const jobController = require('../Controller/jobController')

router.get('/', jobController.getJobs)

router.get('/:id', jobController.getJobId)

router.post('/', jobController.addJob)

router.put('/', jobController.updateJob)

// router.get('/count/:id', companyController.getCountId)

module.exports = router

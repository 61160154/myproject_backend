const express = require('express')
const router = express.Router()
const educationController = require('../Controller/educationController')

router.get('/', educationController.getEducations)

router.get('/:id', educationController.getEducationId)

router.post('/', educationController.addEducation)

router.put('/', educationController.updateEducation)

// router.get('/count/:id', companyController.getCountId)

module.exports = router
